﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace EVS_Support_Tool.Doc_Retriever
{
    class ButtonActionsDocRetrieval
    {

        private ListBox _lstBx = new ListBox();

        private Microsoft.Win32.SaveFileDialog _docRetrieverSaveFileDialog = new Microsoft.Win32.SaveFileDialog();

        public void AcquireSQLDataAndFillListboxes(ListBox fname, ListBox doc, ListBox isDel, TextBox txtbNr)
        {
            string _query;
            string connectionString_documentHeader = "XXX";


            _query = @"select
                      dh.name,
		              dt.name,
                      dh.isDeleted,
                      dh.DocumentId

                    from
                      DocumentHeader dh
                      join [LCYPRDDBT-DOC01\DOC].[EVSDocumentStore].dbo.[DocumentStore] dost on dost.documentid = dh.documentid
                      left join taskDocument td on td.DocumentId = dost.documentid
                      left join task t on t.TaskId = td.taskid                
                      left join documenttype dt on dt.documenttypeid = dh.documenttypeid
                      left join InvestigationTask invt on invt.taskid = t.taskid
                      left join Investigation i on i.InvestigationId = invt.InvestigationId
                    where

                      i.caseNumber = '" + (txtbNr.Text).Trim() + @"'

                    union

                    select
                      dh.name,
		                    dt.name,
                      dh.isDeleted,
                      dh.DocumentId
                    from
                      investigation i
                      left join investigationdocument ind on ind.investigationid = i.investigationid
                      left join documentheader dh on dh.documentid = ind.documentid
                      left join [LCYPRDDBT-DOC01\DOC].[EVSDocumentStore].dbo.[DocumentStore] dos on dos.documentid = dh.documentid
                      left join documenttype dt on dt.documenttypeid = dh.documenttypeid
                    where

                      i.caseNumber = '" + (txtbNr.Text).Trim() + "' ORDER BY dh.Name";

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString_documentHeader))
                {
                    using (SqlCommand command = new SqlCommand(_query, connection))
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {


                                fname.Items.Add(reader.GetValue(0).ToString());
                                doc.Items.Add(reader.GetValue(1).ToString());
                                isDel.Items.Add(reader.GetValue(2).ToString());
                                _lstBx.Items.Add(reader.GetValue(3).ToString());
                            }
                        }
                    }
                }
            }

            catch (Exception LoadErrException)
            {
                MessageBox.Show(LoadErrException.Message, "An error occurred while loading SQL query...", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        public void GetFilenameAndExtension(ListBox fname)
        {
            string _fileExt;
            string _fileName;

            _fileExt = fname.SelectedItem.ToString();
            _fileExt = _fileExt.Substring(_fileExt.LastIndexOf('.'));

            _fileName = fname.SelectedItem.ToString();
            _fileName = _fileName.Substring(0, _fileName.LastIndexOf('.'));

            _docRetrieverSaveFileDialog.FileName = _fileName;
            _docRetrieverSaveFileDialog.DefaultExt = _fileExt;

            // Show FileSaveDialog window and execute method if "Save" is pressed
            if (_docRetrieverSaveFileDialog.ShowDialog() == true)
            {
                SaveSelectedFileUsingSaveFileDialog();
            }
        }

        private void SaveSelectedFileUsingSaveFileDialog()
        {
            string tempStr;
            string selectedLineDocumentID;


            string connectionString_DocumentStore = "XXX";

            _lstBx.SelectedIndex = UserControl_DocumentRetriever.currentlySelectedListBoxIndex;

            selectedLineDocumentID = _lstBx.SelectedItem.ToString(); // Get documentID from previous query and save it to a new variable

            string query = @"select 
		            Base64Data
		            from DocumentStore
		            where DocumentId = '" + selectedLineDocumentID + "'";

            try
            {

                using (var conn = new SqlConnection(connectionString_DocumentStore))
                using (var cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = @"select 
		            Base64Data
		            from DocumentStore
		            where DocumentId = '" + selectedLineDocumentID + "'";

                    tempStr = cmd.ExecuteScalar().ToString();
                    conn.Close();
                }


                byte[] bytes = Convert.FromBase64String(tempStr);

                System.IO.FileStream stream = new FileStream(Path.GetFullPath(_docRetrieverSaveFileDialog.FileName), FileMode.CreateNew);
                System.IO.BinaryWriter writer = new BinaryWriter(stream);

                writer.Write(bytes, 0, bytes.Length);
                writer.Close();
            }

            catch (Exception LoadErrException)
            {
                MessageBox.Show(LoadErrException.Message, "An error occurred while generating file...", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
