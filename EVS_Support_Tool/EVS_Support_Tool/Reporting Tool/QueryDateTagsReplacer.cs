﻿using System;
using System.Text;
using System.Windows.Controls;

namespace EVS_Support_Tool.Reporting_Tool
{
    /// <summary>
    /// reportType - daily, weekly, monthly or custom
    /// 
    /// tagType - #StartDate# or #EndDate# tags inside the SQL query, which must be replaced with DateTime values according 
    ///            to the report type (e.g. for w (weekly) - the range from last week start until end)
    ///
    /// </summary>
    /// 

    class QueryDateTagsReplacer
    {
        public string ReplaceDateTagsWithDateValues(string report, bool calendarOpenedDateTaken, string rawQuery, DatePicker datePickStart, DatePicker datePickEnd)
        {
            //Class Instance
            ConfigTagType configTagType = new ConfigTagType();

            string reportType;
            string tagType;
            string output = "";

            DateTime dateStart;
            DateTime dateEnd;

            StringBuilder sb;

            reportType = configTagType.ReadTagContent(report);

            dateStart = DateTime.MinValue;
            dateEnd = DateTime.MinValue;

            sb = new StringBuilder(rawQuery);


            if (rawQuery.Contains("#StartDate#"))
            {
                tagType = "#StartDate#";

                if (!calendarOpenedDateTaken)
                {
                    dateStart = ReturnDateValueForDatePickers(reportType, tagType);
                    datePickStart.SelectedDate = dateStart;
                }

                else
                {
                    dateStart = (DateTime)datePickStart.SelectedDate;

                }
            }

            if (rawQuery.Contains("#EndDate#"))
            {
                tagType = "#EndDate#";

                if (!calendarOpenedDateTaken)
                {
                    dateEnd = ReturnDateValueForDatePickers(reportType, tagType);
                    datePickEnd.SelectedDate = dateEnd;
                }

                else
                {
                    dateEnd = (DateTime)datePickEnd.SelectedDate;

                }
            }

            output = sb
                .Replace("#StartDate#", dateStart.ToString("yyyy-MM-dd"))
                .Replace("#EndDate#", dateEnd.ToString("yyyy-MM-dd"))
                .ToString();


            calendarOpenedDateTaken = false;

            return output;
        }

        public DateTime ReturnDateValueForDatePickers(string reportType, string tagType)
        {
            // Class Instances
            DatesCalculations datesCalculations = new DatesCalculations();

            DateTime output = DateTime.MinValue;

            if (tagType == "#StartDate#")
            {
                switch (reportType)
                {
                    case "daily":

                        // If today is Monday, then we need to head for 3 days before, starting the report from Friday
                        output = DateTime.Now.Date.DayOfWeek == DayOfWeek.Monday ?
                            datesCalculations.GetFridayDayInCaseOfTodayIsMonday() : datesCalculations.GetYesterdayDay();

                        break;

                    case "weekly":

                        output = datesCalculations.GetFirstDayOfPreviousWeek();
                        break;

                    case "monthly":

                        output = datesCalculations.GetFistdayOfPreviousMonth();
                        break;
                }
            }

            else if (tagType == "#EndDate#")
            {
                switch (reportType)
                {
                    case "daily":

                        output = datesCalculations.GetYesterdayDay();
                        break;

                    case "weekly":

                        output = datesCalculations.GetLastDayOfPreviousWeek();
                        break;

                    case "monthly":

                        output = datesCalculations.GetLastdayOfPreviousMonth();
                        break;
                }
            }

            return output;
        }



        public void ReplaceSQLQueryDataTagsWhileReportCreation(string report, DatePicker dpStart, DatePicker dpEnd, TextBox tb1, TextBox tb2)
        {
            // Class Instances      
            ConfigTagSheet1SQL configTagSheet1SQL = new ConfigTagSheet1SQL();
            ConfigTagSheet2SQL configTagSheet2SQL = new ConfigTagSheet2SQL();

            bool calendarOpenedDateTaken;

            string queryWithoutTagsReplacedSheet1;
            string queryWithoutTagsReplacedSheet2;
            string queryWithTagsReplacedSheet1;
            string queryWithTagsReplacedSheet2;

            // Only true when calendar is used to pickup the date and never here. Refers only to CalendarClosed event
            calendarOpenedDateTaken = false;

            // Gets the RAW query with date tags, e.g. #StartDate# or #EndDate#
            queryWithoutTagsReplacedSheet1 = configTagSheet1SQL.ReadTagContent(report);

            // Gets UPDATED query with date tags, e.g. #StartDate# or #EndDate#
            queryWithTagsReplacedSheet1 = ReplaceDateTagsWithDateValues(
                     report, calendarOpenedDateTaken, queryWithoutTagsReplacedSheet1, dpStart, dpEnd);

            tb1.Text = queryWithTagsReplacedSheet1;

            queryWithoutTagsReplacedSheet2 = configTagSheet2SQL.ReadTagContent(report);
            queryWithTagsReplacedSheet2 = "";

            if (queryWithoutTagsReplacedSheet2 != "")
            {
                queryWithTagsReplacedSheet2 = ReplaceDateTagsWithDateValues(
                report, calendarOpenedDateTaken, queryWithoutTagsReplacedSheet2, dpStart, dpEnd);

                tb2.Text = queryWithTagsReplacedSheet2;
            }
        }
    }

    class DatesCalculations
    {
        // Day
        public DateTime GetTodayDay()
        {
            return DateTime.Now;
        }

        // Day
        public DateTime GetYesterdayDay()
        {
            return DateTime.Now.AddDays(-1);
        }

        // Day
        public DateTime GetFridayDayInCaseOfTodayIsMonday()
        {
            return DateTime.Now.AddDays(-3);
        }

        // Week
        public DateTime GetFirstDayOfPreviousWeek()
        {
            return DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 6);
        }

        // Week
        public DateTime GetLastDayOfPreviousWeek()
        {
            return DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek);
        }

        // Month
        public DateTime GetFistDayOfCurrentMonth()
        {
            DateTime month = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            return month;
        }

        // Month
        public DateTime GetFistdayOfPreviousMonth()
        {
            DateTime month = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            return month.AddMonths(-1);
        }

        // Month
        public DateTime GetLastdayOfPreviousMonth()
        {
            DateTime month = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            return month.AddDays(-1);
        }
    }
}
