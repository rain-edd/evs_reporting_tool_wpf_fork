﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace EVS_Support_Tool.CI_Retriever
{
    class ValidityChecksForClientAndCITextBlocks
    {
        public void ClientNameValidation(string con, string query, TextBlock tbCliFound)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            tbCliFound.Foreground = Brushes.Blue;
                            tbCliFound.Text = "Client Found!";
                        }

                        else
                        {
                            tbCliFound.Foreground = Brushes.Red;
                            tbCliFound.Text = "Client not found!";
                        }
                    }                 
                }
            }
        }

        public void CountCIWordingsAmount(string con, string query, TextBlock tbCICount)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            int rows = (int)reader.GetValue(0);

                            if (rows == 0)
                            {
                                tbCICount.Foreground = Brushes.Red;
                                tbCICount.Text = "No Customized CI found!";
                            }

                            else
                            {
                                tbCICount.Foreground = Brushes.Blue;
                                tbCICount.Text = $"Customized CI wordings total: {rows}";
                            }
                        }
                    }
                }
            }
        }
    }
}
