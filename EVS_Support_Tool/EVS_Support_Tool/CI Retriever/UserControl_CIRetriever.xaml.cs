﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace EVS_Support_Tool.CI_Retriever
{
    /// <summary>
    /// Interaction logic for UserControl_CIWRRetriever.xaml
    /// </summary>
    public partial class UserControl_CIWRRetriever : UserControl
    {
        // Class Instances
        ButtonsActionsCI buttonActionsCI = new ButtonsActionsCI();


        public UserControl_CIWRRetriever()
        {
            InitializeComponent();

            buttonGetSelected_CIWR.IsEnabled = false;
            buttonSaveToFile_CIWR.IsEnabled = false;
        }

        private void TextboxClientName_CIWR_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            textboxClientName_CIWR.Text = "";
        }

        private void ButtonReset_CIWR_Click(object sender, RoutedEventArgs e)
        {
            richTextboxBody_CIWR.Selection.Text = "";

            buttonGetSelected_CIWR.IsEnabled = false;
            buttonSaveToFile_CIWR.IsEnabled = false;
            buttonSearch_CIWR.IsEnabled = true;

            comboBoxLocale_CIWR.Items.Clear();

            listBoxDef_CIWR.Items.Clear();
            listBoxCustomSLA_CIWR.Items.Clear();

            checkBoxDefault_CIWR.IsChecked = false;
            checkBoxCustomSLA_CIWR.IsChecked = false;

            textboxClientName_CIWR.Text = "Acme corporation";

            textBlockClientFound_CIWR.Text = "";
            textBlockCIFoundCount_CIWR.Text = "";
        }

        

        private void ButtonSearch_CIWR_Click(object sender, RoutedEventArgs e)
        {
            buttonActionsCI.FillListBoxesWithSQLData(textboxClientName_CIWR, comboBoxLocale_CIWR);
            buttonActionsCI.ShowClientValidationAndWordingCountForTextBlocks(textboxClientName_CIWR, textBlockClientFound_CIWR, textBlockCIFoundCount_CIWR);
        }

        private void CheckBoxDefault_CIWR_Checked(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < listBoxDef_CIWR.Items.Count; i++)
            {
                listBoxDef_CIWR.SelectAll();
            }
        }

        private void CheckBoxDefault_CIWR_Unchecked(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < listBoxDef_CIWR.Items.Count; i++)
            {
                listBoxDef_CIWR.UnselectAll();
            }
        }

        private void CheckBoxCustomizedSLA_CIWR_Checked(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < listBoxCustomSLA_CIWR.Items.Count; i++)
            {
                listBoxCustomSLA_CIWR.SelectAll();
            }
        }

        private void CheckBoxCustomizedSLA_CIWR_Unchecked(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < listBoxCustomSLA_CIWR.Items.Count; i++)
            {
                listBoxCustomSLA_CIWR.UnselectAll();
            }
        }

        private void ButtonGetSelected_CIWR_Click(object sender, RoutedEventArgs e)
        {
            // Enable/Disable buttons
            buttonSaveToFile_CIWR.IsEnabled = true;

            richTextboxBody_CIWR.Selection.Text = ""; // Erase all text

            string stringDefaultCIWordings = "###############  DEFAULT WORDINGS FOR ALL SLA  ###############";

            // Adding generated wordings content to the RichTextBox
            stringDefaultCIWordings += buttonActionsCI.ConvertHtmlDefaultCIWordingsIntoText(listBoxDef_CIWR, comboBoxLocale_CIWR, textboxClientName_CIWR);
            richTextboxBody_CIWR.Selection.Text += stringDefaultCIWordings;


            string stringCustomizedSLAWordings = "\n\n###############  CUSTOMIZED SLA WORDINGS (OVERWRITE DEFAULTS)  ###############";
            // Adding generated wordings content to the RichTextBox
            stringCustomizedSLAWordings += buttonActionsCI.ConvertHtmlCustomizedSLACIWordingsIntoText(listBoxCustomSLA_CIWR, comboBoxLocale_CIWR, textboxClientName_CIWR);
            richTextboxBody_CIWR.Selection.Text += stringCustomizedSLAWordings;

            // Make lines in richtextbox bolded or colored
            buttonActionsCI.HighlightTextLinesInRichtextbox(richTextboxBody_CIWR);
        }

        private void ButtonSaveToFile_CIWR_Click(object sender, RoutedEventArgs e)
        {
            buttonActionsCI.SaveFileToDirectory(textboxClientName_CIWR, comboBoxLocale_CIWR, richTextboxBody_CIWR);
            buttonSaveToFile_CIWR.IsEnabled = false;
        }

        private void ComboBoxLocale_CIWR_DropDownClosed(object sender, System.EventArgs e)
        {
            buttonGetSelected_CIWR.IsEnabled = true;
            buttonSearch_CIWR.IsEnabled = false;

            buttonActionsCI.ComboboxLocaleActions(listBoxDef_CIWR, listBoxCustomSLA_CIWR, comboBoxLocale_CIWR, textboxClientName_CIWR);
        }
    }
}
