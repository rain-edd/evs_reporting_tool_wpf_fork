﻿using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace EVS_Support_Tool.CI_Retriever
{
    class ButtonsActionsCI
    {
        // Class Instances
        Queries queries = new Queries();


        public void FillListBoxesWithSQLData(TextBox txtBoxCli, ComboBox comboLocale)
        {
            string queryLoadClientName;
            string queryLoadLocale;
            string connectionString_EVS45 = queries.GetConnectString();

            // Loading query for Client name from database
            queryLoadClientName = queries.GetClientName(txtBoxCli);

            // Loading query for languages from database available for this client
            queryLoadLocale = queries.GetLocale(txtBoxCli);


            // Adding data to the listBox
            using (SqlConnection connection = new SqlConnection(connectionString_EVS45))
            {
                using (SqlCommand command = new SqlCommand(queryLoadLocale, connection))
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            comboLocale.Items.Add(reader.GetValue(0).ToString());
                        }
                    }
                }
            }

            // Open Combobox list after the client search
            comboLocale.IsDropDownOpen = true;
        }

        public void ShowClientValidationAndWordingCountForTextBlocks(TextBox txtBoxCli, TextBlock tblCliFound, TextBlock tblCIFound)
        {
            // Class Instances
            ValidityChecksForClientAndCITextBlocks validityChecks = new ValidityChecksForClientAndCITextBlocks();

            string queryLoadClientName;
            string queryRowsCount;
            string connectionString_EVS45 = queries.GetConnectString();

            // Loading query for Client name from database
            queryLoadClientName = queries.GetClientName(txtBoxCli);

            // Validating client to represent the matching result inside the label as notification
            validityChecks.ClientNameValidation(connectionString_EVS45, queryLoadClientName, tblCliFound);

            // Loading query for total number of wordings for available for this client
            queryRowsCount = queries.GetRowsCount(txtBoxCli);

            // Count how many (or zero) CI wordings for this client we have
            validityChecks.CountCIWordingsAmount(connectionString_EVS45, queryRowsCount, tblCIFound);
        }

        public void ComboboxLocaleActions(ListBox lbDef, ListBox lbCustSl, ComboBox comb, TextBox txtbCli)
        {
            string currentlyChosenLocale;
            string queryDefaultWordings;
            string queryCustomSLAWordings;
            string connectionString_EVS45 = queries.GetConnectString(); // Initialize connection string

            lbDef.Items.Clear();
            lbCustSl.Items.Clear();

            // Language string loaded if Client has customized wordings for any language
            currentlyChosenLocale = comb.Text;
            currentlyChosenLocale = comb.SelectedItem.ToString();

            // Loading query to check default wordings we have for this client
            queryDefaultWordings = queries.GetDefaultWordingsRelatedHTMLData(txtbCli, currentlyChosenLocale);

            // Adding data to listbox        
            using (SqlConnection connection = new SqlConnection(connectionString_EVS45))
            {
                using (SqlCommand command = new SqlCommand(queryDefaultWordings, connection))
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            lbDef.Items.Add(reader.GetValue(0));
                        }
                    }
                }
            }

            // Loading query to check customized SLA wordings we have for this client
            queryCustomSLAWordings = queries.GetCustomSLAWordingsRelatedHTMLData(txtbCli, currentlyChosenLocale);

            // Adding data to listbox    
            using (SqlConnection connection = new SqlConnection(connectionString_EVS45))
            {
                using (SqlCommand command = new SqlCommand(queryCustomSLAWordings, connection))
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            lbCustSl.Items.Add(reader.GetValue(0).ToString());
                        }
                    }
                }
            }
        }


        public string ConvertHtmlDefaultCIWordingsIntoText(ListBox lbDef, ComboBox comb, TextBox txtbCli)
        {
            string tempString = "";

            string connectionString_EVS45 = queries.GetConnectString();
            string currentLocale = comb.SelectedValue.ToString(); // Language string (e.g. "English")

            // Determine if there are any records selected. If selected, loop through all these items
            if (lbDef.SelectedItems.Count != 0)
            {
                string pageType = "";
                string query;

                for (int i = 0; i <= lbDef.SelectedItems.Count - 1; i++)
                {
                    pageType += lbDef.SelectedItems[i].ToString();

                    // Loading query to check default wordings we have for this client
                    query = queries.GetDefaultWordingsBodyHTML(txtbCli, currentLocale, pageType);

                    // Addings items to listbox
                    using (SqlConnection connection = new SqlConnection(connectionString_EVS45))
                    {
                        using (SqlCommand command = new SqlCommand(query, connection))
                        {
                            connection.Open();

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    tempString += "\n\nPAGE TYPE:   ";
                                    tempString += reader.GetValue(1).ToString() + "\n";

                                    tempString += "PAGE NUMBER:   ";
                                    tempString += reader.GetValue(2).ToString() + "\n";

                                    tempString += "SUBJECT:   ";
                                    tempString += reader.GetValue(4).ToString() + "\n";

                                    System.Windows.Forms.WebBrowser wb = new System.Windows.Forms.WebBrowser();

                                    wb.Navigate("about:blank");
                                    wb.Document.Write(reader.GetValue(5).ToString()); // Reading data from SQL query, only column 6
                                    wb.Document.ExecCommand("SelectAll", false, null); // Converting HTML to RTF
                                    wb.Document.ExecCommand("Copy", false, null); // And copying the result to clipboard

                                    tempString += System.Windows.Forms.Clipboard.GetText(); // Adding the CI wording body to other stuff (headers etc..)
                                }
                            }
                        }
                    }
                    pageType = "";
                }
            }

            return tempString;
        }


        public string ConvertHtmlCustomizedSLACIWordingsIntoText(ListBox lbCustSl, ComboBox comb, TextBox txtbCli)
        {
            string tempString = "";

            string connectionString_EVS45 = queries.GetConnectString();
            string currentLocale = comb.SelectedValue.ToString(); // Language string (e.g. "English")

            // Determine if there are any records selected. If selected, loop through all these items
            if (lbCustSl.SelectedItems.Count != 0)
            {
                string pageType = "";
                string query;

                for (int i = 0; i <= lbCustSl.SelectedItems.Count - 1; i++)
                {
                    pageType += lbCustSl.SelectedItems[i].ToString();

                    // Loading query to check custom SLA wordings we have for this client
                    query = queries.GetCustomSLAWordingsBodyHTML(txtbCli, currentLocale, pageType);


                    // Addings items to listbox
                    using (SqlConnection connection = new SqlConnection(connectionString_EVS45))
                    {
                        using (SqlCommand command = new SqlCommand(query, connection))
                        {
                            connection.Open();

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    tempString += "\n\nSERVICE LEVEL:   ";
                                    tempString += reader.GetValue(0).ToString() + "\n";

                                    tempString += "PAGE TYPE:   ";
                                    tempString += reader.GetValue(1).ToString() + "\n";

                                    tempString += "PAGE NUMBER:   ";
                                    tempString += reader.GetValue(2).ToString() + "\n";

                                    tempString += "SUBJECT:   ";
                                    tempString += reader.GetValue(4).ToString() + "\n";

                                    System.Windows.Forms.WebBrowser wb = new System.Windows.Forms.WebBrowser();

                                    wb.Navigate("about:blank");
                                    wb.Document.Write(reader.GetValue(5).ToString()); // Reading data from SQL query, only column 6
                                    wb.Document.ExecCommand("SelectAll", false, null); // Converting HTML to RTF
                                    wb.Document.ExecCommand("Copy", false, null); // And copying the result to clipboard

                                    tempString += System.Windows.Forms.Clipboard.GetText(); // Adding the CI wording body to other stuff (headers etc..)
                                }
                            }
                        }
                    }
                    pageType = "";
                }
            }

            return tempString;
        }



        public void SaveFileToDirectory(TextBox txtbCli, ComboBox comb, RichTextBox rtb)
        {
            Microsoft.Win32.SaveFileDialog ciRetrieverSaveFileDialog = new Microsoft.Win32.SaveFileDialog
            {
                FileName = txtbCli.Text + " " + comb.Text.ToUpper(),
                DefaultExt = "rtf",
                AddExtension = true
            };

            // Show FileSaveDialog window and execute method if "Save" is pressed
            if (ciRetrieverSaveFileDialog.ShowDialog() == true)
            {
                TextRange tr = new TextRange(rtb.Document.ContentStart, rtb.Document.ContentEnd);
                using (FileStream fStream = new FileStream(ciRetrieverSaveFileDialog.FileName, FileMode.Create))
                tr.Save(fStream, DataFormats.Rtf);  // Save with richtext formatting
            }
        }

        public void HighlightTextLinesInRichtextbox(RichTextBox rtb)
        {
            // Richtextbox paragraphs highlighting
            foreach (var item in rtb.Document.Blocks)
            {
                var text = new TextRange(item.ContentStart, item.ContentEnd).Text;

                if (text.StartsWith("PAGE") || text.StartsWith("SUBJECT"))
                {
                    item.FontWeight = FontWeights.Bold;
                }


                if (text.StartsWith("SERVICE LEVEL:  "))
                {
                    item.Foreground = Brushes.Red;
                }
            }
        }
    }
}
