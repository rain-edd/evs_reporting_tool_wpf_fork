﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EVS_Support_Tool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            ButtonOpenMenu.Visibility = Visibility.Visible;
        }

        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UserControl usc = null;
            GridMain.Children.Clear();

            switch (((ListViewItem)((ListView)sender).SelectedItem).Name)
            {
                case "Reports":
                    usc = new Reporting_Tool.UserControl_Reports();
                    GridMain.Children.Add(usc);
                    this.programName.Text = "EVS REPORTING TOOL";
                    break;

                case "ciRetriever":
                    usc = new CI_Retriever.UserControl_CIWRRetriever();
                    GridMain.Children.Add(usc);
                    this.programName.Text = "CI WORDINGS RETRIEVER";
                    break;

                case "docRetriever":
                    usc = new Doc_Retriever.UserControl_DocumentRetriever();
                    GridMain.Children.Add(usc);
                    this.programName.Text = "DOCUMENT RETRIEVER";
                    break;

                case "addModify":
                    usc = new Reporting_Tool.UserControl_AddModify();
                    GridMain.Children.Add(usc);
                    this.programName.Text = "EVS REPORTING TOOL";
                    break;

                case "parser":
                    usc = new Reporting_Tool.UserControl_PAR();
                    GridMain.Children.Add(usc);
                    this.programName.Text = "BASE64DATA PARSER";
                    break;
            }
        }

        private void HelpReadmePopupBox_Click(object sender, RoutedEventArgs e)
        {
            HelpWindow hw = new HelpWindow();
            hw.Show();
        }


        private void CloseButtonMain_Click(object sender, RoutedEventArgs e)
        {

            // Class Instance          
            Reporting_Tool.Directories directories = new Reporting_Tool.Directories();     

            directories.DeleteFilesInOutputFilesDirectory(); // Delete all generated reports

            Application.Current.Shutdown();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Drag window
            MouseLeftButtonDown += delegate
            {
                DragMove();
            };
        }


        private void KeyDownF1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                HelpReadmePopupBox_Click(sender, e);
            }
        }
    }
}
